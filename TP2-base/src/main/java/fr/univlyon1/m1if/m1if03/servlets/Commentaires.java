package fr.univlyon1.m1if.m1if03.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.Commentaire;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet(name = "commentaires", urlPatterns = {})
public class Commentaires extends HttpServlet {
    HashMap<String, Groupe> groupes = new HashMap<>();

    @Override
    public void init(ServletConfig config) {
        groupes = (HashMap<String, Groupe>) config.getServletContext().getAttribute("groupes");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (validParameters(request, "groupeId", "billetId")) {
            String groupeId = (String) request.getAttribute("groupeId");
            int billetId = (Integer) request.getAttribute("billetId");
            Billet currentBillet = groupes.get(groupeId).getGestionnaire().getBillet(billetId);
            String comment = request.getParameter("commentaire");
            currentBillet.setCommentaire(comment);
            sendJson(response, getAllCommentairesFromBillet(currentBillet));
        }
        /*String[] urlParsed = (String[]) request.getAttribute("urlParsed");
        Billet currentBillet = groupes.get(urlParsed[2]).getGestionnaire().getBillet(Integer.parseInt(urlParsed[4]));
        if (urlParsed.length == 6) {
            String comment = request.getParameter("commentaire");
            currentBillet.setCommentaire(comment);
            sendJson(response, getAllCommentairesFromBillet(currentBillet));
        } else {
            response.sendError(400, "Pas de paramètres acceptables dans la requête");
        }*/
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(validParameters(request, "groupeId", "billetId")) {
            String groupeId = (String) request.getAttribute("groupeId");
            int billetId = (Integer) request.getAttribute("billetId");
            Billet currentBillet = groupes.get(groupeId).getGestionnaire().getBillet(billetId);
            if (requestAttributeExiste(request, "commentaireId")) {
                int commentaireId = (Integer) request.getAttribute("commentaireId");
                if (currentBillet.hasCommentaire(commentaireId)) {
                    sendJson(response, getCommentaireById(currentBillet, commentaireId));
                } else {
                    response.sendError(404, "Commentaire non trouvé");
                }
            } else {
                sendJson(response, getAllCommentairesFromBillet(currentBillet));
            }

        }
    }


    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(validParameters(request, "groupeId", "billetId")) {
            String groupeId = (String) request.getAttribute("groupeId");
            int billetId = (Integer) request.getAttribute("billetId");
            int commentaireId = (Integer) request.getAttribute("commentaireId");

            String comment = null;//request.getParameter("commentaire");
            BufferedReader reader = request.getReader();
            StringBuilder builder = new StringBuilder();

            String all = getValues(builder, reader);
            String[] allArray = all.split("&");
            for (String couple: allArray) {
                String[] couples = couple.split("=");
                for (int i = 0 ; i < couples.length ; i += 2){
                    if (couples[i].equals("description")){
                        comment = couples[i + 1];
                    }
                }
            }

            Billet currentBillet = groupes.get(groupeId).getGestionnaire().getBillet(billetId);
            updateCommentaire(commentaireId, comment, currentBillet, response);

        } else {
            response.sendError(400, "Pas de paramètres acceptables dans la requête");
        }
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(validParameters(request,"groupeId", "billetId")) {
            String groupeId = (String) request.getAttribute("groupeId");
            int billetId = (Integer) request.getAttribute("billetId");
            Billet currentBillet = groupes.get(groupeId).getGestionnaire().getBillet(billetId);
            int commentaireId = (Integer) request.getAttribute("commentaireId");
            if (currentBillet.hasCommentaire(commentaireId)) {
                deleteCommentaire(commentaireId, currentBillet);
                //sendJson(response, getCommentaireById(currentBillet, commentaireId));
            } else {
                response.sendError(404, "Commentaire non trouvé");
            }
        } else {
            response.sendError(400, "Pas de paramètres acceptables dans la requête");
        }

        /*String[] urlParsed = (String[]) request.getAttribute("urlParsed");
        Billet currentBillet = groupes.get(urlParsed[2]).getGestionnaire().getBillet(Integer.parseInt(urlParsed[4]));
        if (urlParsed.length == 7) {
            if (hasCommentaire(Integer.parseInt(urlParsed[6]), currentBillet)) {
                deleteCommentaire(Integer.parseInt(urlParsed[6]), currentBillet);
            } else {
                response.sendError(404, "Commentaire non trouvé");
            }
        }*/

    }

    private boolean requestAttributeExiste(HttpServletRequest request, String attribute){
        try {
            int test = (int) request.getAttribute(attribute);
            return true;
        } catch (NullPointerException nfe){
            return false;
        }
    }

    protected void deleteCommentaire(int id, Billet billet) {
        billet.getCommentaire().remove(id);
    }

    protected void updateCommentaire(int id, String commentaire, Billet billet, HttpServletResponse response) throws IOException {
        if (hasCommentaire(id, billet)) {
            response.getWriter().println("Im in update");
            billet.updateCommentaire(id, commentaire);
        } else {
            if(billet.getCommentaireSize() == id){
                billet.setCommentaire(commentaire);
            } else {
                response.sendError(404, "Commentaire non trouvé");
            }
        }
    }

    private String getValues(StringBuilder builder, BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }

    protected ArrayList<Commentaire> getAllCommentairesFromBillet(Billet billet) {
        return billet.getCommentaire();
    }

    protected Commentaire getCommentaireById(Billet billet, int idCom) {
        return billet.getCommentaireById(idCom);
    }

    protected boolean hasCommentaire(int id, Billet billet) {
        return billet.hasCommentaire(id);
    }

    protected boolean valid(String[] urlParsed) {
        try {
            int id = Integer.parseInt(urlParsed[7]);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    protected boolean validParameters(HttpServletRequest request,String groupeId, String billetId){
        try {
            String testGroupeId = (String) request.getAttribute(groupeId);
            int testBilletId = (int) request.getAttribute(billetId);
            if (groupes.containsKey(testGroupeId)) {
                return groupes.get(testGroupeId).hasBillet(testBilletId);
            } else return false;
        } catch (NullPointerException e) {
            return false;
        }
    }

    protected void sendJson(HttpServletResponse response, Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().println(mapper.writeValueAsString(obj));

    }
}