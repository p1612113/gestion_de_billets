package fr.univlyon1.m1if.m1if03.classes;

import java.util.ArrayList;

public class Groupe {
    private String nom, description, proprietaire;
    private ArrayList<String> participants;
    private GestionBillets gestionnaire;

    public Groupe(String nom, String description, String proprietaire, ArrayList<String> participants, GestionBillets gestionnaire) {
        this.nom = nom;
        this.description = description;
        this.proprietaire = proprietaire;
        this.participants = participants;
        this.gestionnaire = gestionnaire;
    }
    public Groupe(String nom, String description, String proprietaire) {
        this.nom = nom;
        this.description = description;
        this.proprietaire = proprietaire;
        this.participants = new ArrayList<String>() ;
        this.gestionnaire = new GestionBillets();
    }


    public Groupe() {
        this.nom = "Rien";
        this.description = "Vide";
        this.proprietaire = "Personne";
        this.participants = new ArrayList<String>() ;
        this.gestionnaire = new GestionBillets();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }

    public ArrayList<String> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<String> participants) {
        this.participants = participants;
    }

    public void addParticipant (String name) {
        this.participants.add(name);
    }

    public GestionBillets getGestionnaire() {
        return gestionnaire;
    }

    public void setGestionnaire(GestionBillets gestionnaire) {
        this.gestionnaire = gestionnaire;
    }

    public boolean hasParticipant(String pseudo) { return participants.contains(pseudo); }

    public boolean hasBillet(int billetId) { return gestionnaire.hasBillet(billetId);}
}
