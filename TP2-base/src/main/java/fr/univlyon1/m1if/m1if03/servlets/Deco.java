package fr.univlyon1.m1if.m1if03.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
// import java.io.PrintWriter;

@WebServlet(name = "Deco", urlPatterns = "/Deco")
public class Deco extends HttpServlet {
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session  = request.getSession();
        session.removeAttribute("pseudo");
        session.removeAttribute("gestionnaire");
        session.invalidate();                               
        String pageToForward = request.getContextPath();

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");   
        response.sendRedirect(pageToForward);
        
    }

}
