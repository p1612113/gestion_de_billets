package fr.univlyon1.m1if.m1if03.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m1if.m1if03.classes.Groupe;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet(name = "groupes", urlPatterns = {})
public class Groupes extends HttpServlet {
    HashMap<String, Groupe> groupes = new HashMap<>();

    @Override
    public void init(ServletConfig config) {
        groupes = (HashMap<String, Groupe>) config.getServletContext().getAttribute("groupes");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupeId = (String) request.getAttribute("groupeId");

        if ( !isNullOrEmpty(groupeId) ){
            if(groupes.containsKey(groupeId)) {
                this.sendJson(response, getGroupeById(groupeId));
                response.setStatus(200); // SUCCESSFUL OPERATION
            } else {
                response.sendError(404, "Groupe non trouvé");
            }
        } else {
            sendJson(response, getAllGroupes());
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupe = (String) request.getParameter("nom");
        String description = (String) request.getParameter("description");
        String proprietaire = (String) request.getParameter("proprietaire");

        if ( !isNullOrEmpty(groupe) ){
            addGroupe(groupe, description, proprietaire);
            response.setStatus(201); // GROUPE CREE
        } else {
            response.sendError(400 , "GROUPES : Pas de paramètres acceptables dans la requête");
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupeId = (String) request.getAttribute("groupeId");
        String description = null; //(String) request.getParameter("description");
        String proprietaire = null; //(String) request.getParameter("proprietaire");
        BufferedReader reader = request.getReader();
        StringBuilder builder = new StringBuilder();

        String all = getValues(builder, reader);
        String[] allArray = all.split("&");
        for (String couple: allArray) {
            String[] couples = couple.split("=");
            for (int i = 0 ; i < couples.length ; i += 2){
                if (couples[i].equals("description")){
                    description = couples[i + 1];
                }
                if (couples[i].equals("proprietaire")){
                    proprietaire = couples[i + 1];
                }
            }
        }

        if ( !isNullOrEmpty(groupeId) ){
            updatePet(groupeId, description, proprietaire, response);
        } else {
            response.sendError(400, "Pas de paramètres acceptables dans la requête");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupeId = (String) request.getAttribute("groupeId");

        if ( !isNullOrEmpty(groupeId) ){
            if( groupes.containsKey(groupeId) ) {
                deleteGroupe(groupeId);
                response.setStatus(204);
            } else {
                response.sendError(404, "Groupe non trouvé");
            }
        } else {
            response.sendError(400, "Pas de paramètres acceptables dans la requête");
        }
    }

/**
 * Fonctions du swagger
 */
    private String getValues(StringBuilder builder, BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString().replace("%20", " ");
    }


    private void  deleteGroupe(String groupeId){
        groupes.remove(groupeId);
    }

    private void updatePet(String groupeId, String desc, String prop, HttpServletResponse response){
        if (groupes.containsKey(groupeId)) {
            groupes.get(groupeId).setDescription(desc);
            groupes.get(groupeId).setProprietaire(prop);
            response.setStatus(204); // CORRECTEMENT CREE OU MODIFIE
        } else {
            groupes.put(groupeId, new Groupe(groupeId, desc, prop));
            response.setStatus(404); // groupe non trouve
        }
    }

    private Groupe getGroupeById(String id){
        return this.groupes.get(id);
    }

    private void addGroupe(String groupe, String desc, String prop) {
        this.groupes.put(groupe, new Groupe(groupe, desc, prop));
    }

    private HashMap<String, Groupe> getAllGroupes(){
        return this.groupes;
    }

    protected boolean isNullOrEmpty(String str){
        return str == null || str.equals("");
    }

    protected boolean testParamsInvalid(String nom, String desc, String prop) {
        return (nom == null || nom.equals("") || desc == null || desc.equals("") || prop == null || prop.equals(""));
    }

    protected void sendJson( HttpServletResponse response, Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
//        response.setContentType("application/Json");
        response.getWriter().println(mapper.writeValueAsString(obj));

    }

}

