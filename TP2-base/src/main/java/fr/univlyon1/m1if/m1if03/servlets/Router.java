package fr.univlyon1.m1if.m1if03.servlets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;
import fr.univlyon1.m1if.m1if03.classes.Parser;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

//@WebServlet(name = "Router", urlPatterns = "/*")
public class Router extends HttpServlet {
    HashMap<String, Groupe> groupes = new HashMap<>();
    ArrayList<String> utilisateurs = new ArrayList<>();
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        config.getServletContext().setAttribute("groupes", groupes);
        config.getServletContext().setAttribute("utilisateurs", utilisateurs);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doRouting(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doRouting(request, response);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doRouting(request, response);
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doRouting(request,response);
    }

    protected void doRouting(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getPathInfo();
        PrintWriter out = response.getWriter();
        Parser parsed = new Parser(url);

        if (valid(url)) {
            parsed.parserUrl();
            String[] urlParsed = parsed.getParsedUrl();
            int length = urlParsed.length;
            //request.setAttribute("urlParsed", urlParser);
            if(length >= 1 ){
                if(urlParsed[1].equals("groupes")){
                    if(length >= 3){
                        String groupeId = urlParsed[2];
                        request.setAttribute("groupeId", groupeId);
                        if (length >= 4 && urlParsed[3].equals("billets")){
                            if (length >= 5){
                                int billetId = Integer.parseInt(urlParsed[4]);
                                request.setAttribute("billetId", billetId);
                                if(length >= 6 && urlParsed[5].equals("commentaires")){
                                    if (length == 7){
                                        int commentaireId = Integer.parseInt(urlParsed[6]);
                                        request.setAttribute("commentaireId", commentaireId);
                                        getServletContext().getNamedDispatcher("commentaires").forward(request, response);
                                    } else {
                                        getServletContext().getNamedDispatcher("commentaires").forward(request, response);
                                    }
                                } else {
                                    getServletContext().getNamedDispatcher("billets").forward(request, response);
                                }
                            } else {
                                getServletContext().getNamedDispatcher("billets").forward(request, response);
                            }
                        } else {
                            getServletContext().getNamedDispatcher("groupes").forward(request, response);
                        }
                    } else {
                        getServletContext().getNamedDispatcher("groupes").forward(request, response);
                    }
                }

                if (urlParsed[1].equals("users")) {
                    if(length ==3 ){
                        if(urlParsed[2].equals("login")) {
                            request.setAttribute("userStatus", "login");
                        } else if (urlParsed[2].equals("logout")) {
                            request.setAttribute("userStatus", "logout");
                        }
                    }
                    getServletContext().getNamedDispatcher("users").forward(request, response);
                }
            }
        }
    }

    public boolean valid(String url) {
        return !(url == null || url.equals("/"));
    }
}