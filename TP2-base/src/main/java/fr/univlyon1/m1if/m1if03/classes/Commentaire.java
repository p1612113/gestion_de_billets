package fr.univlyon1.m1if.m1if03.classes;

public class Commentaire {
    private int id;
    private String auteur, texte;

    public Commentaire(){

    }
    public Commentaire(int idCom, String aut, String tex){
        this.id = idCom;
        this.auteur = aut;
        this.texte = tex;
    }

    public Commentaire(String aut, String tex){
        this.auteur = aut;
        this.texte = tex;
    }
    public Commentaire(String tex){
        this.texte = tex;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }


    public int getId() {
        return id;
    }

    public String getAuteur() {
        return auteur;
    }

    public String getTexte() {
        return texte;
    }

}
