package fr.univlyon1.m1if.m1if03.classes;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;

public class Billet {
    private String titre, contenu, auteur;
    private ArrayList<Commentaire> commentaires;
    private int id;

    public Billet() {
        this.titre = "";
        this.contenu = "";
        this.auteur = "";
        //this.commentaires = new HashMap<>() ;
    }

    public Billet(String titre, String contenu, String auteur) {
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
    }

    public Billet(String titre, String contenu) {
        this.titre = titre;
        this.contenu = contenu;
        this.commentaires = new ArrayList<>();
    }

    public ArrayList<Commentaire> getCommentaire() {
        if (commentaires.isEmpty()) {

        }
        return commentaires;
    }



    public void setCommentaire(String commentaire) { this.commentaires.add(new Commentaire(this.commentaires.size(), auteur, commentaire)); }

    public String getTitre() {
        return titre;
    }

    @JsonIgnore
    public int getCommentaireSize() {
        if (this.commentaires.isEmpty()) {
            return 0;
        } else {
            return this.commentaires.size();
        }
    }

    @JsonIgnore
    public Commentaire getCommentaireById (int id) {
        return commentaires.get(id);
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public void updateCommentaire(int id, String commentaire) {
        commentaires.get(id).setTexte(commentaire);
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public String toString() {
        return "Author = " + auteur + "; Title = " + titre + "; Content = " + contenu;
    }
    public boolean hasCommentaire(int idCommentaire) {
        return (idCommentaire>=0 && idCommentaire<=commentaires.size() -1);
    }
    @JsonIgnore
    public boolean isEmpty() { return auteur == "" && titre == "" && contenu == "" &&  commentaires.isEmpty();}

}
