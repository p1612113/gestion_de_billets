package fr.univlyon1.m1if.m1if03.servlets;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m1if.m1if03.classes.Groupe;
import java.lang.Object;
import javax.json.Json;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.HashMap;

@WebServlet(name = "users", urlPatterns = {})
public class Users  extends HttpServlet {
    ArrayList<String> utilisateurs;
    //HMAC

    @Override
    public void init(ServletConfig config) {
        utilisateurs = (ArrayList<String>) config.getServletContext().getAttribute("utilisateurs");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        sendJson(response, utilisateurs);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(requestAttributeExiste(request, "userStatus")) {
            String userStatus = (String) request.getAttribute("userStatus");
            if (userStatus.equals("logOut")){
                if(request.getParameterMap().containsKey("pseudo")) {
                    String pseudo = request.getParameter("pseudo");
                    logOutUser(pseudo);
                 }
            } else if(userStatus.equals("login")) {
                if(request.getParameterMap().containsKey("pseudo")) {
                    String pseudo = request.getParameter("pseudo");
                    loginUser(pseudo);
                }
            }
        }
    }

    protected void loginUser(String pseudo){
    }

    protected void logOutUser(String pseudo){

    }

    protected void sendJson(HttpServletResponse response, Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().println(mapper.writeValueAsString(obj));
    }

    private boolean requestAttributeExiste(HttpServletRequest request, String attribute){
        try {
            String test = (String) request.getAttribute(attribute);
            return true;
        } catch (NullPointerException nfe){
            return false;
        }
    }
}
