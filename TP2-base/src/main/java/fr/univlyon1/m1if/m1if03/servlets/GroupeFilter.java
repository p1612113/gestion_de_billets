package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@WebFilter(filterName = "Groupes")
public class GroupeFilter implements Filter {

    private ServletContext context;

    public void init( FilterConfig config ) throws ServletException {
        this.context = config.getServletContext();
    }

    public void doFilter( ServletRequest req, ServletResponse res, FilterChain chain ) throws IOException,
            ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        HttpSession session = request.getSession();
        if (request.getMethod().equalsIgnoreCase ("GET") && request.getParameter("nomGroupe") != null) {
            HashMap<String, Groupe> groupes = (HashMap<String, Groupe>) context.getAttribute("groupes");
            String pseudo = (String) session.getAttribute( "pseudo" );
            String nomGroupe = request.getParameter( "nomGroupe" );
            if (  groupes.containsKey(nomGroupe) ) {
                if ( groupes.get(nomGroupe).hasParticipant(pseudo)){
                    chain.doFilter(request,response);
                } else {
                    response.setContentType("text/html");
                    PrintWriter out = response.getWriter();
                    out.println("<HTML>");
                    out.println("<HEAD>");
                    out.println("<TITLE>");
                    out.println("Pas authorise");
                    out.println("</TITLE>");
                    out.println("</HEAD>");
                    out.println("<BODY>");
                    out.println("<H1>Pas membre du groupe</H1>");
                    out.println("Desole. Life is hard.");
                    out.println("</BODY>");
                    out.println("</HTML>");
                    out.close();
                }
            } else {
                request.getRequestDispatcher("WEB-INF/jsp/groupe.jsp").forward(request, response);
            }

        } else {
            chain.doFilter(request, response);
        }
    }

    public void destroy() {
    }
}