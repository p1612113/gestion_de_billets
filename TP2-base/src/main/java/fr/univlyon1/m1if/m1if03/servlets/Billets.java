package fr.univlyon1.m1if.m1if03.servlets;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.HashMap;

@WebServlet(name = "billets", urlPatterns = {})
public class Billets extends HttpServlet {
    private HashMap<String, Groupe> groupes = new HashMap<>();

    @Override
    public void init(ServletConfig config) {
        groupes = (HashMap<String, Groupe>) config.getServletContext().getAttribute("groupes");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupeId = (String) request.getAttribute("groupeId");

        if (groupes.containsKey(groupeId)){
            if (requestAttributeExiste(request, "billetId")) {
                int billetId = (int) request.getAttribute("billetId");
                if (groupes.get(groupeId).hasBillet(billetId)) {
                    sendJson(response, getBilletById(groupeId, billetId));
                    response.setStatus(200);
                } else {
                    response.sendError(404 , "Billet non trouve");
                }
            } else {
                response.getWriter().println("Im in");
                sendJson(response, getAllBilletsFromGroupe(groupeId));
                response.setStatus(200);
            }
        } else {
            response.sendError(400 , "BILLETS : Pas de paramètres acceptables dans la requête [groupe does not exist]");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupeId = (String) request.getAttribute("groupeId");

        if (groupes.containsKey(groupeId)){
            String titre = request.getParameter("titre");
            String contenu = request.getParameter("contenu");
            addBillet(groupeId, new Billet(titre, contenu));
        } else {
            response.sendError(400 , "BILLETS : Pas de paramètres acceptables dans la requête [groupe does not exist]");
        }
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String groupeId = (String) request.getAttribute("groupeId");
        int billetId = (int) request.getAttribute("billetId");
        String titre = null;
        String contenu = null;
        BufferedReader reader = request.getReader();
        StringBuilder builder = new StringBuilder();

        String all = getValues(builder, reader);
        String[] allArray = all.split("&");
        for (String couple: allArray) {
            String[] couples = couple.split("=");
            for (int i = 0 ; i < couples.length ; i += 2){
                if (couples[i].equals("titre")){
                    titre = couples[i + 1];
                }
                if (couples[i].equals("contenu")){
                    contenu = couples[i + 1];
                }
            }
        }
        if (groupes.containsKey(groupeId) && billetId >= 0){
            Groupe groupe = groupes.get(groupeId);
            if(groupe.hasBillet(billetId)) {
                updateBillet(groupeId, billetId, titre, contenu);
            } else if (billetId == groupe.getGestionnaire().size()) {
                groupe.getGestionnaire().add(billetId,new Billet(titre, contenu));
            }

        } else {
            response.sendError(400 , "BILLETS : Pas de paramètres acceptables dans la requête [groupe does not exist]");
        }
    }

    private String getValues(StringBuilder builder, BufferedReader reader) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();

    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String groupeId = (String) request.getAttribute("groupeId");
        int billetId = (int) request.getAttribute("billetId");

        if (groupes.containsKey(groupeId) && billetId >= 0){
            deleteBillet(groupeId, billetId);
            response.setStatus(204);
        } else {
            response.sendError(400 , "BILLETS : Pas de paramètres acceptables dans la requête [groupe does not exist]");
        }
    }

    private boolean requestAttributeExiste(HttpServletRequest request, String attribute){
        try {
            int test = (int) request.getAttribute(attribute);
            return true;
        } catch (NullPointerException nfe){
            return false;
        }
    }

    private void deleteBillet(String groupeId, int billetId) {
        groupes.get(groupeId).getGestionnaire().getBillets().remove(billetId);
    }

    private void updateBillet(String groupeId, int billetId, String titre, String contenu) {
        Groupe groupe = groupes.get(groupeId);
        groupe.getGestionnaire().getBillet(billetId).setTitre(titre);
        groupe.getGestionnaire().getBillet(billetId).setContenu(contenu);
    }

    protected void addBillet(String groupeId, Billet billet){
        groupes.get(groupeId).getGestionnaire().add(billet);
    }

    protected GestionBillets getAllBilletsFromGroupe(String nbGroupe) {
        return this.groupes.get(nbGroupe).getGestionnaire();
    }

    protected boolean validParameters(String groupeId, int billetId){
        if (groupes.containsKey(groupeId)) {
            return groupes.get(groupeId).hasBillet(billetId);
        }
        return false;
    }

    protected void sendJson( HttpServletResponse response, Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().println(mapper.writeValueAsString(obj));
    }

    private Billet getBilletById(String groupeId, int billetId){
        return  groupes.get(groupeId).getGestionnaire().getBillet(billetId);
    }

}
