package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.GestionBillets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

@WebServlet(name = "GroupeControleur", urlPatterns = "/GroupeControleur")
public class GroupeControleur extends HttpServlet {

    HashMap<String, Groupe> groupes = new HashMap<>();

    @Override
    public void init(ServletConfig config){
        config.getServletContext().setAttribute("groupes", groupes);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nom = request.getParameter("nom");
        String description = request.getParameter("description");
        String participants[] = request.getParameterValues("participants");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession(true);

        if(nom != null && !nom.equals("") && description != null && !description.equals("") && participants != null) {

            if (!groupes.containsKey(nom)){
                ArrayList<String> p = new ArrayList<>();
                Collections.addAll(p, participants);
                Groupe groupe = new Groupe();
                groupe.setNom(nom);
                groupe.setDescription(description);
                groupe.setParticipants(p);
                groupe.setGestionnaire(new GestionBillets());
                groupes.put(nom, groupe);
            }
            session.setAttribute("gestionnaire", groupes.get(nom).getGestionnaire());
            request.getRequestDispatcher("WEB-INF/jsp/menu.jsp").forward(request, response);
        } else {
            response.setContentType("text/html");
            out.println("<HTML>");
            out.println("<HEAD>");
            out.println("<TITLE>");
            out.println("Pas authorise");
            out.println("</TITLE>");
            out.println("</HEAD>");
            out.println("<BODY>");
            out.println("<H1>Il faudra remplir tous les champs.</H1>");
            out.println("Desole. Life is hard.");
            out.println("</BODY>");
            out.println("</HTML>");
            out.close();
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nomGroupe = request.getParameter("nomGroupe");
        HttpSession session = request.getSession(true);

        if(nomGroupe != null) {
            session.setAttribute("gestionnaire", groupes.get(nomGroupe).getGestionnaire());
            request.getRequestDispatcher("WEB-INF/jsp/menu.jsp").forward(request, response);
        }
    }
}
