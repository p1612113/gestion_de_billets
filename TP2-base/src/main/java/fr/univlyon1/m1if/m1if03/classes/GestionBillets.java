package fr.univlyon1.m1if.m1if03.classes;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GestionBillets implements Serializable{

    private static final long serialVersionUID = -5900146520487021460L;
    private List<Billet> billets;
    @JsonIgnore
    private int idBilletChoisit;

    public GestionBillets() {
        this.billets = new ArrayList<>();
        this.idBilletChoisit = -1;
    }

    public int getIdBilletChoisit() {
        return idBilletChoisit;
    }

    public void setIdBilletChoisit(int idBilletChoisit) {
        this.idBilletChoisit = idBilletChoisit;
    }

    public List<Billet> getBillets() {return billets;}

    public void setBillets(List<Billet> billets) { this.billets = billets; }

    public void add(Billet billet) { billet.setId(size()); this.billets.add(billet); }

    public void add(int id, Billet billet) { billet.setId(id); this.billets.add(id, billet); }

    public Billet getBillet(int i) { return billets.get(i); }

    public Billet choisit() { return idBilletChoisit < 0 ? new Billet() : billets.get(idBilletChoisit); }

    public boolean testPourBillet() { return (getIdBilletChoisit() > -1) && (getIdBilletChoisit() < size()); }

    public int size() { return billets.size(); }

    public String toString(){ return "liste" + billets.toString(); }

    public  boolean emptyList () { return !this.billets.isEmpty(); }

    public boolean hasBillet(int billetId) {
        return billets == null ? false : billetId > -1 && billetId < billets.size();
    }
}
