package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Billet;
import fr.univlyon1.m1if.m1if03.classes.GestionBillets;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import fr.univlyon1.m1if.m1if03.classes.GestionBillets;
import fr.univlyon1.m1if.m1if03.classes.Groupe;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@WebServlet(name = "Controleur", urlPatterns = "/Controleur")
public class Controleur extends HttpServlet {

    HashMap<String, Groupe> groupeList = new HashMap<>();

    @Override
    public void init(ServletConfig config){
        groupeList = (HashMap<String, Groupe>) config.getServletContext().getAttribute("groupeList");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        GestionBillets gestionnaire = (GestionBillets) session.getAttribute("gestionnaire");

        if (request.getParameter("contenu") != null || request.getParameter("titre") != null) {
            Billet billet = new Billet();
            billet.setContenu(request.getParameter("contenu"));
            billet.setTitre(request.getParameter("titre"));
            billet.setAuteur((String) session.getAttribute("pseudo"));
            gestionnaire.add(billet);
        }

        if(request.getParameter("commentaire") != null && !request.getParameter("commentaire").equals("") && request.getParameter("index") != null){
            int i =  Integer.parseInt(request.getParameter("index"));
            gestionnaire.getBillet(i).setCommentaire(request.getParameter("commentaire"));
        }

        request.getRequestDispatcher("WEB-INF/jsp/menu.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        GestionBillets gestionnaire = (GestionBillets) session.getAttribute("gestionnaire");
        Cookie tableDeCookie[] = request.getCookies();
        PrintWriter out = response.getWriter();

        if(request.getParameter("idSelect") != null ){
            int idSelect = Integer.parseInt(request.getParameter("idSelect"));
            boolean numBilletExiste = false;
            boolean cookieMajNumBillets = false;
            boolean idBilletChoisit = false;
            boolean cookieMajIdBillet = false;
            boolean numComExiste = false;
            boolean cookieMajNumCom = false;

            for(Cookie cookie : tableDeCookie){
                if(cookie.getName().equals("numBillet")){
                    numBilletExiste = true;
                    if(!cookie.getValue().equals(Integer.toString(gestionnaire.size()))){
                        cookie.setValue(Integer.toString(gestionnaire.size()));
                        response.addCookie(cookie);
                        cookieMajNumBillets = true;
                    }
                }
                if(cookie.getName().equals("idBilletChoisit")) {
                    idBilletChoisit = true;
                    if (!cookie.getValue().equals(Integer.toString(idSelect))) {
                        cookie.setValue(Integer.toString(idSelect));
                        response.addCookie(cookie);
                        cookieMajIdBillet = true;
                    }
                }
                if(cookie.getName().equals("numCom")){
                    numComExiste = true;
                    if(!cookie.getValue().equals(Integer.toString(gestionnaire.getBillet(idSelect).getCommentaireSize()))){
                        cookie.setValue(Integer.toString(gestionnaire.getBillet(idSelect).getCommentaireSize()));
                        response.addCookie(cookie);
                        cookieMajNumCom = true;
                    }
                }
            }
            if (!numBilletExiste) {
                Cookie ckNumBillet = new Cookie("numBillet",Integer.toString(gestionnaire.size()));
                response.addCookie(ckNumBillet);
                cookieMajNumBillets = true;
            }
            if (!idBilletChoisit) {
                Cookie ckIdBillet = new Cookie("idBilletChoisit",Integer.toString(idSelect));
                response.addCookie(ckIdBillet);
                cookieMajIdBillet = true;
            }
            if (!numComExiste) {
                Cookie ckCom = new Cookie("numCom",Integer.toString(gestionnaire.getBillet(idSelect).getCommentaireSize()));
                response.addCookie(ckCom);
                cookieMajNumCom = true;
            }
            if ( cookieMajIdBillet || cookieMajNumBillets || cookieMajNumCom || idSelect == 1) {
                gestionnaire.setIdBilletChoisit(idSelect);
                request.getRequestDispatcher("WEB-INF/jsp/menu.jsp").forward(request, response);
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            }

        }


    }
}
