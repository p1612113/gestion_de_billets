package fr.univlyon1.m1if.m1if03.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import java.io.IOException;
import java.util.ArrayList;


@WebServlet(name = "Init", urlPatterns = "/client/Init")
public class Init extends HttpServlet {
    ArrayList<String> utilisateurs = new ArrayList<>();
    //    ServletContext contexte;
    @Override
    public void init(ServletConfig config){
        config.getServletContext().setAttribute("utilisateurs", utilisateurs);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pseudo = request.getParameter("pseudo");

        if(pseudo != null && !pseudo.equals("")) {
            HttpSession session = request.getSession(true);
            session.setAttribute("pseudo", pseudo);
            if ( !utilisateurs.contains(pseudo)){
                utilisateurs.add(pseudo);
            }
//            request.getRequestDispatcher("WEB-INF/jsp/groupe.jsp").forward(request, response);
        } else {
            response.sendRedirect("index.html");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("index.html");
    }
}
