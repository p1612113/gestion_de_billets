$(document).ready( function () {

    /***************************************************
     ********************* GET *************************
     ***************************************************/

    function display(id) {
        $('section').hide();
        $(id).show();
    }

    function templating(id_template, data, id_html) {
        let template = $(id_template).html();
        Mustache.parse(template);
        $(id_html).html(Mustache.render(template, data));
    }

    function setHeader(xhr, settings) {
        xhr.setRequestHeader('Authorization', window.sessionStorage.getItem('token'));
        xhr.setRequestHeader('accept', 'application/json');
    }

    $(window).on('hashchange', function () {
        var hash = document.location.hash;
        switch (hash) {
            case '#groupes':
                display(hash);
                $.get('https://192.168.75.13/api/v2/groupes', {}, function (data) {
                    templating("#groupesTemplate", {'groupes' : data}, '#groupesContainer');
                }, 'json');                        
                break;
            case '#users' :
                $.ajax({
                    method: "GET",
                    url: 'https://192.168.75.13/api/v2/users',
                    dataType: "json",
                    beforeSend: setHeader,
                    success : function (data) {
                        display(hash);
                        templating("#usersTemplate", {'users' : data}, '#usersContainer');
                    },
                    error : function (xhr, status, error) {
                        display("#index");
                        templating("#errMsgTemplate", {"message" : "Unauthorized To Access Users"}, "#errMsg");
                        setTimeout(function() { $("#errMsg").fadeOut().empty(); }, 3000);            
                    }
                });
                break;
            default:
                display(hash);
                break;
        }
    });

    $('#groupes').on('click','.router-groupe',function() {
        var url = $(this).data('reference');
        $.ajax({
            method: "GET",
            url: url,
            contentType : "application/json",
            beforeSend: setHeader,
            success : function (data) {
                display("#groupe");
                templating("#groupeTemplate", data, '#groupeContainer');
                $("#groupeNav").show();
            },
            error : function (xhr, status, error) {
                display("#groupes");
                templating("#errMsgTemplate", {'message' : "Unauthorized To Access The Group"}, "#errMsg");
                setTimeout(function() { $("#errMsg").fadeOut().empty(); }, 3000);
            }
        });
    });

    $('#groupe').on('click','.router-billet',function() {
        var url = $(this).data('reference');
        $.ajax({
            method: "GET",
            url: url,
            contentType : "application/json",
            beforeSend: setHeader,
            success : function (data) {
                display("#billet");
                templating("#billetTemplate", data, '#billetContainer');
                $("#billetNav").show();
            },
            error : function () {
                display("#groupe");
                templating("#errMsgTemplate", {'message' : "Something Went Wrong"}, "#errMsg");
                setTimeout(function() { $("#errMsg").fadeOut().empty(); }, 3000);
            }
        });
    });

    /***************************************************
     ********************* POST ************************
     ***************************************************/

    $('#userLogin').submit(function (event) {

        event.preventDefault();

        var $form = $(this),
            pseudo = $form.find("input[name='pseudo']").val(),
            url = "https://192.168.75.13/api/v2/users/login";

        var obj = { pseudo : pseudo };

        $.ajax({
            method: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (output, status, xhr) {
                window.sessionStorage.setItem('token', xhr.getResponseHeader("authorization"));
                templating("#succMsgTemplate", {'message' : "You Are Logged In"}, "#succMsg");
                setTimeout(function() { $("#succMsg").fadeOut().empty(); }, 3000);
                window.location.hash = "#groupes";
            }
        });
    });

    $('#userLogout').submit(function (event) {

        event.preventDefault();

        var url = "https://192.168.75.13/api/v2/users/logout";

        $.ajax({
            method: "POST",
            url: url,
            contentType: "application/json",
            beforeSend: setHeader,
            success: function () {
                window.sessionStorage.removeItem('token');
                templating("#succMsgTemplate", {'message' : "You Are Logged Out"}, "#succMsg");
                setTimeout(function() { $("#succMsg").fadeOut().empty(); }, 3000);
            }
        });
    });

    $('#createGroupe').submit(function (event) {

        event.preventDefault();

        var $form = $(this),
            groupe = $form.find("input[name='groupe']").val(),
            url = "https://192.168.75.13/api/v2/groupes";

        var obj = {nom: groupe};

        $.ajax({
            method: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(obj),
            beforeSend: setHeader,
            success: function () {
                $('#groupesList').append(
                    '<li class="list-group-item" >' +
                        '<a class=\'router-groupe\' href=\'javascript:void(0)\' data-reference=\'' + url + '/' + groupe + '\' > ' +
                            url + '/' + groupe +
                        '</a>' +
                    '</li>');
            },
            error : function () {
                display("#index");
                templating("#errMsgTemplate", {'message' : "Unauthorized To Create a Group"}, "#errMsg");
                setTimeout(function() { $("#errMsg").fadeOut().empty(); }, 3000);
            }
        });
    });

    $('#createBillet').submit(function (event) {

        event.preventDefault();

        var $form = $(this),
            groupeName = $("h2[name='groupeName']").text(),
            title = $form.find("input[name='titre']").val(),
            content = $form.find("textearea[name='contenu']").val()
        url = "https://192.168.75.13/api/v2/groupes/" + groupeName + "/billets";

        var obj = {
            titre: title,
            contenu: content
        };

        $.ajax({
            method: "POST",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(obj),
            beforeSend: setHeader,
            success: function () {
                $('#billetsList').append(
                    '<li class="list-group-item">'+
                        '<a class=\'router-billet\' href=\'javascript:void(0)\' data-reference=\''+ url + '/' + title + '\' > ' + 
                        url + '/' + title + 
                        '</a>' +
                    '</li>');
                templating("#succMsgTemplate", {'message' : "Billet Created"}, "#succMsg");
                setTimeout(function() { $("#succMsg").fadeOut().empty(); }, 3000);
            },
            error : function () {
                display("#groupe");
                templating("#errMsgTemplate", {'message' : " Something Went Wrong"}, "#errMsg");
                setTimeout(function() { $("#errMsg").fadeOut().empty(); }, 3000);
            }
        });
    });

});