<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div style="border-style: solid; margin: 4px 4px 4px 4px; padding: 2px 2px 2px 2px;">
    <p> Auteur : ${billet.auteur}   </p>
    <p> Titre : ${billet.titre}     </p>
    <p> Contenu : ${billet.contenu} </p>
    <form method="post" action="Controleur">
        <p>
            <input type="hidden" name="index" value="${billet.id}"/>
            <input type="text" name="commentaire" placeholder="Commentaire...">
            <input type="submit" value="Envoyer">
        </p>
    </form>

    <c:if test="${billet.commentaire != null}">
        <c:set var="commentaires" value="${billet.getCommentaire()}" scope="page"/>
        <ul>
            <c:forEach items = "${commentaires}" var ="commentaire" >
                <li> ${commentaire}</li>
            </c:forEach>
        </ul>
    </c:if>
</div>
