<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="fr.univlyon1.m1if.m1if03.classes.Billet" %>

<jsp:useBean id="gestionnaire" beanName="gestionnaire" scope="session" type="fr.univlyon1.m1if.m1if03.classes.GestionBillets"/>
<!doctype html>
<html>
<head>
    <title>Billet</title>
<%--    <meta http-equiv="refresh" content="5;">--%>
</head>
<body>
<h2>Hello <%= session.getAttribute("pseudo")%> !</h2>
<c:if test="<%= gestionnaire.emptyList() %>">
    <form method="GET" action="Controleur">
        <c:set var="billets" value = "<%=gestionnaire.getBillets()%>"/>
        <select name="idSelect">
        <c:forEach items = "${billets}" var ="billet" >
            <option value="${billets.indexOf(billet)}">
                <c:out value="${billet.titre}"> </c:out>
            </option>
        </c:forEach>
        </select>
        <hr>
        <button type="submit" value="Envoyer">Selectionner</button>
    </form>

    <c:if test="<%= gestionnaire.testPourBillet() %>">
        <c:set var="billet" value="<%=gestionnaire.choisit()%>" scope="request"/>
        <jsp:include page="billet.jsp" >
            <jsp:param name="billet" value="${billet}" />
        </jsp:include>
    </c:if>

</c:if>


<p><a href="saisie.html">Saisir un nouveau billet</a></p>
<p><a href="Deco">Se déconnecter</a></p>

</body>
</html>
