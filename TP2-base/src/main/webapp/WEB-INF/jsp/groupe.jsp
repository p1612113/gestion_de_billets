<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<jsp:useBean id="utilisateurs" beanName="utilisateurs" scope="application" type="java.util.ArrayList"/>
<!doctype html>
<html>
<head>
    <title>Groupes</title>
</head>
<body>
<p><h2>Hello <%= session.getAttribute("pseudo")%> !</h2></p>
<p><h3>Choisi un groupe</h3></p>
<div>
    <form method="GET" action="GroupeControleur">
        <select name="nomGroupe">
            <c:forEach items = "${groupes.keySet()}" var ="groupName" >
                <option value="${groupName}">
                    <c:out value="${groupName}"> </c:out>
                </option>
            </c:forEach>
        </select>
        <button type="submit" value="Selectionner">Selectionner</button>
    </form>
</div>
<p><h3>Ou bien cree un nouveau groupe... </h3></p>
<div>
    <form method="POST" action="GroupeControleur">
        <p> Entrez le nom du groupe : </p>
        <p><input type="text" name="nom"></p>
        <p>Entrez une description du groupe</p>
        <p><textarea name="description" rows="4" cols="50"> </textarea></p>
        <p>Choisisez les participants :</p>
        <p>
        <select multiple name="participants">
            <c:forEach items = "${utilisateurs}" var ="name" >
                <option value="${name}">
                    <c:out value="${name}"> </c:out>
                </option>
            </c:forEach>
        </select>
        </p>
        <p><button type="submit" value="Creer">Creer groupe</button></p>
    </form>
</div>

<p><a href="Deco">Se déconnecter</a></p>

</body>
</html>
