# Preambule
## Etudiants:
- Eyosyas Abebe 
- Michelle Leano Martinet

# TP2 & TP3
## Deploiment

Le lien vers la page d’accueil de l’application déployée sur notre VM est : [ https://192.168.75.18/api/v1 ]

# TP2



### 1. Conception basique de l'application
#### 1.1 Interface de gestion des billets

On a cree un ArrayList<String> pour les commentaires. Et on les a afiche a l'aide des valises ul. 
On a cree la variable globale gestionnaire de type GestionBillets sur billet.jsp, et le refresh dans une valise meta.

#### 1.2 Déconnexion

Pour la deconnexion le servlet Deco est a disposition et il fait une redirection externe. Car comme ca une nouvelle requete est cree.

#### 1.3 Ajout d'un menu

On a trouve plus conveniant que ca soit le fichier menu.jsp qui inclu le billet a etre affiche. 
On a aussi envoye le parametre billet au fichier billet.jsp  en scope request

```php
<c:set var="billet" value="<%=gestionnaire.getBilletChoisit()%>" scope="request"/>
<jsp:include page="billet.jsp" >
    <jsp:param name="billet" value="${billet}" />
</jsp:include>
```
#### 1.4 Ajout de groupes

On a ajoute un input de type text dans index.html et on a ajoute le valeur saisi dans un  attribut de session "goupe".
On a ajoute la liste du groupe au menu 

# TP3

### Refactoring de votre application

#### Pattern contexte
On a cree la classe Groupe avec les getters et setters.
On a cree le HashMap<String, Groupe>
et on a utilise un <jsp:usebean> de type GestionBillets car on a trouve que pour afficher les billets du groupe selectione c'etait plus simple de juste envoyer son gestionnaire

#### MVC Pull-based
On a ajoute un fichier groupe.jsp dans lequel on a mis un formulaire pour la creation d'un groupe en methode POST et une liste des groupes disponibles en methode GET

#### MVC Push-based
On a envoye les donnees necessaires par un scope de request

- Contrôle de l'accès aux vues :
On a mis tous les fichiers dans un dossier WEB-INF/jsp/ et change tous les redirections vers ces fichiers avec des redirections internes.

#### Pattern chaîne de responsabilité
On a free les filtres AuthFilter et GroupeFilter, Authfilter est applique a toute l'application "/*" et GroupeFilter a /GroupeControleur juste.

### Gestion du cache

### Utilisation des cookies
On a remarque les last-modified et if-modified-since
Dans la methode GET du controleur on a gere les cookies pour l'affichage des billets choisis 

# TP4
### Deploiment

Le lien vers la page d’accueil de l’application déployée sur notre VM est : [ https://192.168.75.18/api/v2 ]

### 1. Ressources
On a cree le servlet "Router" qui gere le URI et fait appel au bon servlet a partir de ce qu'il lit. En faissant un getNameDispatcher et un forward de request et response pour le servlet correspondant.
> Ex:  getServletContext().getNamedDispatcher("groupes").forward(request, response); // pour passer la requete a groupes

Pour les cas d'un groupe en specifique ou un billet en specifique on ajoute l'id correspondant dans la requette.
> Ex:  request.setAttribute("groupeId", groupeId);

#### 1.3 Semantique de HTTP
Les methodes GET POST PUT et DELETE fonctionnent tous en suivant le fichier swagger.yaml du TP. Nos test on etait realises a l'aide de Postman.

# TP5 & TP7
### Deploiment

Le lien vers la page d’accueil de l’application déployée sur notre VM est : [ https://192.168.75.18/ ]

# TP5 
On a utilisee l'API fourni par l'enseignant. [ https://192.168.75.13/api/v2 ]

### Requetes utilisees
Requete qui n'as pas besoin d'authentification  
 - https://192.168.75.13/api/v2/groupes

Requete qui n'as pas besoin d'authentification
- https://192.168.75.13/api/v2/groupes/[idgroupe]
- https://192.168.75.13/api/v2/groupes/[idgroupe]/billets
- https://192.168.75.13/api/v2/groupes/[idgroupe]/billets/[idbillet]
- https://192.168.75.13/api/v2/groupes/[idgroupe]/billets/[idbillet]/commentaires
- https://192.168.75.13/api/v2/users/login
- https://192.168.75.13/api/v2/users/logout

### 1. Single-Page Application
Notre SPA est capable de:
- Gerer la connexion et déconnexion d'un utilisateur [Si l'utilisateur n'est pas authentifie il est renvoye ver le accueil ou il existe un formulaire de connexion] 
- Récupérer le token et sauvegarder dans une variable de session, pour le renvoyer serveur 
- Récupérer la liste des groupes
- Créer un groupe [Si authentifie]
- Créer ou récupérer un billet [Si authentifie]

# TP7

### Elements du DOM faisant partie de l’app shell
| Nom du requete             | Type       |
| -------------------------- |:----------:|
| 192.169.75.18 [index.html] | document   |
| style.css                  | stylesheet |
| jqueri.min.js              | script     |
| bootstrap.min.js           | script     |
| bootstrap.min.css          | stylesheet |
| all.min.css                | stylesheet |
| jquery-3.2.1.min.js        | script     |
| mustache.min.js            | script     |
| script.js                  | script     |
| post.js                    | script     |
| get.js                     | script     |

* Les elements decris sont les requetes faits par le navigateur

Notre app shell correspond au moment où le menu et la page de connexion devient visible. 

### Elements du DOM faisant partie du CRP
Les memes que pour l'app shell mais en plus:

| Nom du requete      | Type       |
| ------------------- |:----------:|
| fa-solid-900.wolff2 | font       |
| groupes             | xhr        |
| users               | xhr        |

### 1. Analyse de l'état initial de l'application

#### 1.1 Script pour calculer les temps d'affichage

```javascript
 var t = window.performance.timing;
 //Mesure le temps qu’a pris le navigateur depuis 
 // l’analyse des premiers octets reçus du document HTML et la construction de l'arborescence HTML (DOM).
 var html_time = t.domInteractive - t.domLoading;
 //Mesure le temps qu’a pris le navigateur depuis 
 // l’analyse des premiers octets reçus du document HTML et la construction de l'arborecence d'affichage (DOM et CSSOM)
 var app_shell_time = t.domContentLoadedEventEnd - t.domLoading;
 //Mesure le temps qu’a pris le navigateur depuis 
 // l’analyse des premiers octets reçus du document HTML et la construction de la page complet (requete GET groupes inclu)
 var crp_time = t.domComplete - t.domLoading;
 
 console.log(html_time);
 console.log(app_shell_time);
 console.log(crp_time);
```

 > **domLoading** : c'est l'horodatage de démarrage de la totalité du processus. Le navigateur est sur le point de commencer à analyser les premiers octets reçus pour le document HTML.
 
 > **domInteractive** : indique le moment où le navigateur a terminé d'analyser l'ensemble du code HTML et où la construction du DOM est terminée.
 
 > **domContentLoaded** : indique le moment où le modèle DOM est prêt et où aucune feuille de style n'empêche l'exécution de JavaScript. Cela signifie qu'il est désormais possible (potentiellement) de construire l'arborescence d'affichage.
   [De nombreux logiciels JavaScript attendent cet événement avant de commencer à exécuter leur propre logique. Pour cette raison, le navigateur capture les horodatages EventStart et EventEnd pour nous permettre de savoir combien de temps a duré l'exécution.]
 
 > **domComplete** : comme son nom l'implique, la totalité du traitement est terminée et le téléchargement de toutes les ressources sur la page (images, etc.) est terminé. C'est-à-dire que le bouton fléché en cours de chargement a cessé de tourner.

#### 1.2 Resultat du Calcul

| Calcul              | Temps(ms)  |
| ------------------- |:----------:|
| TempsChargeHTML     | 345        |
| TempsAppShell       | 408        |
| TempsCRP            | 1343       |

### 2. Déploiement des fichiers statiques sur nginx

#### 2.1 Resultat du Calcul

| Calcul              | Temps(ms)  |Amelioration(%) |
| ------------------- |:----------:|:--------------:|
| TempsChargHTML      | 278 ms     | 19.5           |
| TempsAppShell       | 281 ms     | 31.13          |
| TempsCRP            | 333 ms     | 75.3           |

### 3. Optimisation de votre application


|                       | AVANT                                                       | A PRESENT                    |AMELIORATION           |
| --------------------  |-----------------------------------------------------------  |----------------------------  |-----------------------|
| CDN                   | On utilise des CDN au moment d'utiliser bootstrap et jQuery | on n'as pas modifie celle ci | |
| async/defer           | | On utilise l’ attribute “defer” sur les fichiers js puisqu’on a pas besoin du code contenue dans le js qu’après le chargement de la page (DOM et CSSOM) a terminer.| Nouveau Temps : <br> <ul> <li>TempsChargHTML : <b>276ms</b> (Amélioration <b>0.7%</b>)</li> <li>TempsAppShell : <b>275ms</b> (Amélioration <b>2.3%</b>)</li> <li>TempsCRP : <b>316ms</b> (Amélioration <b>5.1%</b>)</li> </ul> |
| ressources critiques  | On avait 3 fichier javaScript: <br> <ul><li> Script.js qui affiche les différents parties de l’application, soit en cliquant sur le menu ou en changeant l’URL. </li><li> Get.js ce charge de récupérer les informations requis par l’ajax. </li><li> Post.js ce charge des formulaires et de la connexion en utilisant le post de ajax. </li></ul> | <ul><li>On a seulement une page js qui contient toutes les fonctions.Ça nous permet de diminuer le nombres de requêtes.</li> <li>On a aussi changer l’affichage pour faire en sorte que la redirection se fait  que quand le hash de la page HTML à  changer.</li></ul> |Nouveau Temps : <br> <ul> <li>TempsChargHTML : <b>271ms</b> (Amélioration <b>1.8%</b>)</li> <li>TempsAppShell : <b>267ms</b> (Amélioration <b>3%</b>)</li> <li>TempsCRP : <b>306ms</b> (Amélioration <b>3.2%</b>)</li> </ul>|
| refactoring app shell | Toute la page se rechargeait avec un rafraichissement même si il n’y a  aucun changement fait. | Nos fichiers de css, script et html sont maintenant conservées en cache pour que quand on rafraichit la page on ne récupère que le partie changer. | Nouveau Temps : <br> <ul> <li>TempsChargHTML : <b>259ms</b> (Amélioration <b>4.4%</b>)</li> <li>TempsAppShell : <b>264ms</b> (Amélioration <b>1.2%</b>)</li> <li>TempsCRP : <b>292ms</b> (Amélioration <b>4.6%</b>)</li> </ul> |



